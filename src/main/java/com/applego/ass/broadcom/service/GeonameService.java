package com.applego.ass.broadcom.service;

import com.applego.ass.broadcom.client.RestClient;
import com.applego.ass.broadcom.model.Geoname;
import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
//@CacheConfig(cacheNames="Place")
public class GeonameService {
    Logger LOGGER = LoggerFactory.getLogger(GeonameService.class);

    //@Autowired
    //private CityRepository revRe;

    @Autowired
    private RestClient restClient;

    @Autowired
    private EmbeddedCacheManager cacheManager;


    //@CacheEvict(allEntries = true)
    public void evictAllEntries(){
        LOGGER.info("---> Evict All Entries.");
        Cache<Object, Object> cache = cacheManager.getCache("RegulatedEntity");
        cache.clear();
    }

    //@CacheEvict(key="#urn")
    public void evictEntry(final String key){
        LOGGER.info("---> Evict Place with geonameId = " + key);

        Cache<Object, Object> cache = cacheManager.getCache("RegulatedEntity");
        cache.evict(key);
    }

    public List<Geoname> findByExactName(final String name) {

        List<Geoname> geonames = restClient.findPlacesByExactName(name, 100); //TODO: Propagate maxEows from filter

        return geonames/*.toArray(new Geoname[] {})*/;
    }
}
