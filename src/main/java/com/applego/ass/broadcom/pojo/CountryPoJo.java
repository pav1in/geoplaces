package com.applego.ass.broadcom.pojo;

import com.applego.ass.broadcom.model.Continent;
import com.applego.ass.broadcom.model.Geoname;

import java.util.List;
import java.util.Set;

public class CountryPoJo {

    public List<GeonamePoJo> getCities() {
        return null;
    }
    private Continent continent;

    private String name;

    private Geoname capital;

    private Set<Geoname> places;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Geoname getCapital() {
        return capital;
    }

    public void setCapital(Geoname capital) {
        this.capital = capital;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }
}
