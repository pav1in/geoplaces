package com.applego.ass.broadcom.pojo;

import com.applego.ass.broadcom.model.Geoname;

import java.util.List;

public class GeonamePoJo {
    private Long id;
    private String adminCode1;       //"adminCode1": "ENG",
    private Double lng;              //"lng": "-0.12574",
    private String geonameId;        //"geonameId": 2643743,
    private String toponymName;      //"toponymName": "London",
    private String countryId;        //"countryId": "2635167",
    private String fcl;              //"fcl": "P",
    private Long   population;       //"population": 7556900,
    private String countryCode;      //"countryCode": "GB",
    private String name;             //"name": "London",
    private String fclName;          //"fclName": "city, village,...",
    private List<String> adminCodes; //"adminCodes1": {
                                     //       "ISO3166_2": "ENG"
                                     //   },
    private String countryName;      //"countryName": "United Kingdom",
    private String fcodeName;        //"fcodeName": "capital of a political entity",
    private String adminName1;       //"adminName1": "England",
    private Double lat;              //"lat": "51.50853",
    private String fcode;            //"fcode": "PPLC"


    public static Geoname fromPoJo(GeonamePoJo body) {
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getAdminCode1() {
        return adminCode1;
    }

    public void setAdminCode1(String adminCode1) {
        this.adminCode1 = adminCode1;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getGeonameId() {
        return geonameId;
    }

    public void setGeonameId(String geonameId) {
        this.geonameId = geonameId;
    }

    public String getToponymName() {
        return toponymName;
    }

    public void setToponymName(String toponymName) {
        this.toponymName = toponymName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getFcl() {
        return fcl;
    }

    public void setFcl(String fcl) {
        this.fcl = fcl;
    }

    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getFclName() {
        return fclName;
    }

    public void setFclName(String fclName) {
        this.fclName = fclName;
    }

    public List<String> getAdminCodes() {
        return adminCodes;
    }

    public void setAdminCodes(List<String> adminCodes) {
        this.adminCodes = adminCodes;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getFcodeName() {
        return fcodeName;
    }

    public void setFcodeName(String fcodeName) {
        this.fcodeName = fcodeName;
    }

    public String getAdminName1() {
        return adminName1;
    }

    public void setAdminName1(String adminName1) {
        this.adminName1 = adminName1;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public String getFcode() {
        return fcode;
    }

    public void setFcode(String fcode) {
        this.fcode = fcode;
    }
}
