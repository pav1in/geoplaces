package com.applego.ass.broadcom.pojo;

import com.applego.ass.broadcom.model.Country;

import java.util.Set;

public class ContinentPoJo {

    private String name;

    public Set<Country> getCountries() {
        return countries;
    }

    public void setCountries(Set<Country> countries) {
        this.countries = countries;
    }

    private Set<Country> countries;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
