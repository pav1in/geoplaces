package com.applego.ass.broadcom.client;
import java.text.MessageFormat;
import java.util.*;

import com.applego.ass.broadcom.model.Geoname;
import com.applego.ass.broadcom.model.SearchFilter;
import com.applego.ass.broadcom.model.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestClient.class);

    private final int DEFAULT_MAXIMUM_RECORDS = 100;

    @Value("${geonames.url.search_by_name}")
    private String searchByNameUrl;

    @Value("${geonames.url.search_by_criteria}")
    private String searchByCriteriaUrl;

    @Value("${geonames.url.find_by_geonameid}")
    private String searchByGeonameId;

    @Value("${geonames.username}")
    private String username;

    /**
     * Find Geoname by all provided SearchFilter criteria
     *
     * @param searchFilter
     * @return
     */
    public List<Geoname> findByCriteria(SearchFilter searchFilter) {
        LOGGER.info("Find palces by exact name");

        String queryString = prepareQuery(searchFilter);
        String searchUrl = MessageFormat.format(searchByCriteriaUrl, queryString, username, searchFilter.getMaxRows());

        return findPlaces(searchUrl);
    }

    private static String prepareQuery(SearchFilter searchFilter) {
        StringBuilder sb = new StringBuilder();
        sb.append("q=");
        if ((searchFilter.getName() != null) && !searchFilter.getName().isEmpty()){
            sb.append(searchFilter.getName());
        }

        /*if ((searchFilter.getCountryName() != null) && !searchFilter.getCountryName().isEmpty()) {
            if (sb.length()>2) {
                sb.append("&");
            }
            sb.append("countryName=").append(searchFilter.getCountryName());
        }*/

        if ((searchFilter.getCountryCode() != null) && !searchFilter.getCountryCode().isEmpty()) {
            if (sb.length()>2) {
                sb.append("&");
            }
            sb.append("country=").append(searchFilter.getCountryCode());
        }

        return sb.toString();
    }


    /**
     * Find Geoname places
     *
     * @param searchUrl
     * @return
     */
    public List<Geoname> findPlaces(final String searchUrl) {
        LOGGER.info("Find palces by exact name");

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<HttpHeaders> httpEntitiy = new HttpEntity("parameters", headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<SearchResult> response = restTemplate.exchange(searchUrl, HttpMethod.GET, httpEntitiy, SearchResult.class);

        List<Geoname> places = null;
        if (HttpStatus.OK.equals(response.getStatusCode())) {

            if (response.getBody() != null) {

                // Copy into new list to avoid access issues
                places = Arrays.asList(response.getBody().getGeonames().toArray(new Geoname[]{}));
                LOGGER.info("Places found: " + places.size());
            } else {
                LOGGER.info("No places found.");
            }
        } else {
            LOGGER.info("Couldn't find place data. Geonames /search service returned Http status code: " + response.getStatusCode());
        }

        if (places == null) {
            places = new ArrayList<Geoname>();
        }
        return places;
    }


    /**
     * Find Geoname by exact name
     *
     * @param exactName
     * @return
     */
    public List<Geoname> findPlacesByExactName(final String exactName, int maxRows) {
        LOGGER.info("Find palces by exact name");

        String searchUrl = MessageFormat.format(searchByNameUrl, exactName, username, maxRows);

        return findPlaces(searchUrl);
    }


    /**
     * Find Geoname by exact name
     *
     * @param geonameId
     * @return
     */
    public List<Geoname> findPlacesByGeonameId(final String geonameId) {
        LOGGER.info("Get Regulated ModelEntity's version data");

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<HttpHeaders> httpEntitiy = new HttpEntity("parameters", headers);

        String urlString = MessageFormat.format(searchByGeonameId, geonameId, username);

        //return findPlaces(urlString);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Geoname> response = restTemplate.exchange(urlString, HttpMethod.GET, httpEntitiy, Geoname.class);
        Geoname place = response.getBody();
        List<Geoname> places = new ArrayList();
        if (place != null) {
            places.add(place);
        }

        return places;
    }


    protected static String getHttpQueryStringFromParams(Map<String, String> queryParams) {
        if ((queryParams == null) || (queryParams.size() == 0)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Iterator keyIt = queryParams.keySet().iterator();

        while (keyIt.hasNext()) {
            String key = (String) keyIt.next();
            String value = queryParams.get(key);
            sb.append(key).append("=").append(value);
            if (keyIt.hasNext()) {
                sb.append("&");
            }
        }
        return sb.toString();
    }

    protected static void setHeadersFromMap(HttpHeaders headers, Map<String, String> headerParams) {
        Iterator keyIt = headerParams.keySet().iterator();
        while (keyIt.hasNext()) {
            String key = (String) keyIt.next();
            String value = headerParams.get(key);
            headers.add(key, value);
        }
    }

}
