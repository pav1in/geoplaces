package com.applego.ass.broadcom.model;

import java.util.List;

public class AdminCodes {
    private List<String> adminCodes;

    public List<String> getAdminCode() {
        return adminCodes;
    }

    public void setAdminCode(List<String> adminCodes) {
        this.adminCodes = adminCodes;
    }
}
