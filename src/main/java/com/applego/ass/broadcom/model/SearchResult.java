package com.applego.ass.broadcom.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResult {

    private Long totalResultsCount;

    private List<Geoname> geonames;

    public SearchResult() {
    }

    public Long getTotalResultsCount() {
        return totalResultsCount;
    }

    public void setTotalResultsCount(Long totalResultsCount) {
        this.totalResultsCount = totalResultsCount;
    }

    public List<Geoname> getGeonames() {
        return geonames;
    }

    public void setGeonames(List<Geoname> geonames) {
        this.geonames = geonames;
    }
}
