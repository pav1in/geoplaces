package com.applego.ass.broadcom.model;

import javax.validation.constraints.*;

public class SearchFilter {

    @NotNull
    @DecimalMin(value="10", message="{error.searchFilter.maxRows.min}")
    @DecimalMax("100")
    private Integer maxRows;

    @NotNull
    private Boolean exactNameOnly;

    @NotNull
    private String name;

    //@Digits()
    //@Pattern(regexp="\\d{6}")
    @Pattern(regexp = ("^$|([0-9]*[1-9]+)"),message="error.searchFilter.geonameId.format")
    private String geonameId;

    private String countryName;

    private String countryCode;


    public Integer getMaxRows() {
        return maxRows;
    }

    public void setMaxRows(Integer maxRows) {
        this.maxRows = maxRows;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getExactNameOnly() {
        return exactNameOnly;
    }

    public void setExactNameOnly(Boolean exactNameOnly) {
        this.exactNameOnly = exactNameOnly;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getGeonameId() {
        return geonameId;
    }

    public void setGeonameId(String geonameId) {
        this.geonameId = geonameId;
    }
}
