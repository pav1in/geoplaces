package com.applego.ass.broadcom.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Country {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Continent continent;

    private String name;

    @OneToOne
    private Municipality capital;

    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER) //mappedBy = "country",
    @JoinColumn(name="country")
    private Set<Municipality> places;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Municipality getCapital() {
        return capital;
    }

    public void setCapital(Municipality capital) {
        this.capital = capital;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }
}
