package com.applego.ass.broadcom.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Continent {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String geonameId;

    @OneToMany(mappedBy = "continent", cascade=CascadeType.ALL, fetch = FetchType.EAGER) // , fetch = FetchType.EAGER Exception in runtime for List Collection: org.hibernate.loader.MultipleBagFetchException: cannot simultaneously fetch multiple bags: :
    private Set<Country> countries;

    public Set<Country> getCountries() {
        return countries;
    }

    public void setCountries(Set<Country> countries) {
        this.countries = countries;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeonameId() {
        return geonameId;
    }

    public void setGeonameId(String geonameId) {
        this.geonameId = geonameId;
    }
}
