package com.applego.ass.broadcom.prsistence;

import com.applego.ass.broadcom.model.Geoname;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CityRepository extends CrudRepository<Geoname, Long> {

    List<Geoname> findByGeonameId(String geonameId);
}