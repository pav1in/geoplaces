package com.applego.ass.broadcom.prsistence;

import com.applego.ass.broadcom.model.Geoname;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<Geoname, Long> {

    Geoname findByGeonameId(String geonameId);
}