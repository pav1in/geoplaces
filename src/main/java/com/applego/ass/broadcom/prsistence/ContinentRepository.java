package com.applego.ass.broadcom.prsistence;

import com.applego.ass.broadcom.model.Continent;
import org.springframework.data.repository.CrudRepository;

public interface ContinentRepository extends CrudRepository<Continent, Long> {

    Continent[] findByGeonameId(String name);
}