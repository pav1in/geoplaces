package com.applego.ass.broadcom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.builder.SpringApplicationBuilder;
//import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 * Debug with:  -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8000
 *              -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=1044
 */
@SpringBootApplication
@EnableCaching
@EnableScheduling
public class GeoPlacesApp  extends SpringBootServletInitializer {

	public static void main(String args[]) {
		SpringApplication.run(GeoPlacesApp.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(GeoPlacesApp.class);
	}
}
