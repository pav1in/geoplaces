package com.applego.ass.broadcom.controller;

import com.applego.ass.broadcom.model.SearchFilter;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class SearchFilterValidator implements Validator {
    /**
     * This Validator validates *just* Person instances
     */
    public boolean supports(Class clazz) {
        return SearchFilter.class.equals(clazz);
    }

    public void validate(Object obj, Errors e) {
        SearchFilter searchFilter = (SearchFilter) obj;
        /*if (searchFilter.getMaxRows() < 10) {
            e.rejectValue("maxRows", "error.searchFilter.maxRows.min");
        } else if (searchFilter.getMaxRows() > 100) {
            e.rejectValue("maxRows", "error.searchFilter.maxRows.max");
        }*/

        if (Boolean.TRUE.equals(searchFilter.getExactNameOnly())) {
            if ((searchFilter.getName() == null) || searchFilter.getName().isEmpty()) {
                e.rejectValue("name", "error.searchFilter.name.empty");
            }
        }
        //ValidationUtils.rejectIfEmpty(e, "name", "name.empty");
    }

}
