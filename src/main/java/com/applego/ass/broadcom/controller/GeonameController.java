package com.applego.ass.broadcom.controller;
import java.util.*;

import com.applego.ass.broadcom.client.RestClient;
import com.applego.ass.broadcom.model.Geoname;
import com.applego.ass.broadcom.model.SearchFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


//@Rest
@Controller
public class GeonameController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeonameController.class);


    @Autowired
    private SearchFilterValidator validator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(validator);
    }


    @Autowired
    private RestClient geoPlaceRestClient;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(Model model) {
        if (!model.containsAttribute("searchFilter")) {
            SearchFilter searchFilter = new SearchFilter();
            searchFilter.setMaxRows(10);
            searchFilter.setExactNameOnly(false);
            model.addAttribute("searchFilter", searchFilter);
        }

        return "search";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchPlaces(Model model, @Validated @ModelAttribute SearchFilter searchFilter, BindingResult bindingResult) {
        /*if ( searchFilter == null) {
            model.addAttribute("errorMessage", "No places named '" + searchFilter.getName() + "' were found.");
        } else {*/
        if (bindingResult.hasErrors()) {
            LOGGER.info("BINDING RESULT ERRORS FOUND");
        } else {
            List<Geoname> places = new ArrayList();
            if ((searchFilter.getGeonameId() != null ) && !searchFilter.getGeonameId().isEmpty()) {
                places = geoPlaceRestClient.findPlacesByGeonameId(searchFilter.getGeonameId());
            } else if (Boolean.TRUE.equals(searchFilter.getExactNameOnly())) {
                if ((searchFilter.getName()==null)  || searchFilter.getName().isEmpty()){
                    model.addAttribute("errorMessage", "Place name is required if getExactNameOnly is checked.");
                } else {
                    places = geoPlaceRestClient.findPlacesByExactName(searchFilter.getName(), searchFilter.getMaxRows());
                }
            } else {
                places = geoPlaceRestClient.findByCriteria(searchFilter);

            }

            if ((places == null) || places.isEmpty()){
                model.addAttribute("errorMessage", "No places named '" + searchFilter.getName() + "' were found.");
            } else {
                model.addAttribute("places", places);
            }
        }

        return "search";
    }

    @RequestMapping(value = "/places", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity findPlaces(@Valid @RequestBody(required = true) SearchFilter searchFilter) {
        List<Geoname> places = new ArrayList();
        if ((searchFilter == null) || (null == searchFilter.getExactNameOnly())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Search filter exactNameOnly field is mandatory..");
        }

        if (Boolean.TRUE.equals(searchFilter.getExactNameOnly()))
            places = geoPlaceRestClient.findPlacesByExactName(searchFilter.getName(), searchFilter.getMaxRows());

        //findPlaces(searchFilter);
        //form.setMaxRows(10);
        //form.setExactNameOnly(true);
        //return new ResponseEntity(model, HttpStatus.OK);
        return new ResponseEntity<>(places, HttpStatus.OK);
    }
}
