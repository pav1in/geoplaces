/*
 * Copyright 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applego.ass.broadcom;

import static org.assertj.core.api.Assertions.assertThat;

import com.applego.ass.broadcom.client.RestClient;
import com.applego.ass.broadcom.controller.GeonameController;
import com.applego.ass.broadcom.model.Geoname;
import com.applego.ass.broadcom.model.SearchFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import java.io.*;
import java.net.URISyntaxException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlacesTests {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlacesTests.class);

	@Autowired
	private RestClient restClient;

	@Autowired
	private GeonameController geonameController;


	@Test
	public void geoNamesSmokeTest() throws IOException, URISyntaxException {

		assertThat(geonameController).isNotNull();

		assertThat(restClient).isNotNull();


	}

	@Test
	public void geoNamesControllerTest() throws IOException, URISyntaxException {
		String aName = "Praha";

		SearchFilter searchFilter = new SearchFilter();
		searchFilter.setName(aName);
		searchFilter.setExactNameOnly(Boolean.TRUE);

		Model model = new ExtendedModelMap();
		BindingResult bRes = new BeanPropertyBindingResult(searchFilter, "searchFilter");
		String result = geonameController.searchPlaces(model, searchFilter, bRes);
		assertThat(result).isNotBlank();
		assertThat(result).isEqualTo("search");
		//assertThat(model).

		/*for (Geoname gn1: geonames) {
			Geoname gn2 = getTestEntitiesFromFile(gn1.getGeonameId());
			assert(gn2 != null);
			assert(gn1.getGeonameId().equals(gn2.getGeonameId()));
			assert(gn1.getCountryName().equals(gn2.getCountryName()));
			assert(gn1.getFcode().equals(gn2.getFcode()));
			assert(gn1.getFcl().equals(gn2.getFcl()));
		}*/
	}

}
