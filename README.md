# geoplaces - Broadcom assignment

Backlog:

DONE  1. Application properties not displayed in thymeleaf pages
DONE  2. Navigation - clarify: /findPlaces dos not map to the controller RequestMapping
WIP:  3. Error page is used to exception display messages. A error div is more appropriate
DONE  4. Would be nice to have the map on the same page with the search Filter
DONE: 5.  FE - ThymeLeafe
TODO: 5.5 FE - VUE/Angular
TODO: 6.  Other controller should return REST for JS integration
MUST: 7.  Deployment on AWS or Google:
WIP:  8.  Implement MaxRows and clarify data displayed - population, isCapital, etc
REJ:  9.  Implement data model
REJ: 10. Setup Infinispan Cache to retrieve previous data. Switch cache by checkbox.
TODO: 12. Copyright
TODO: 13. GitLab CI pipelines

TODO: Java8 on AWS
TODO: Return back service to provide different searches
TODO: TAble beautify


onclick="addMarkerForPlace(&#39;{
}&#39;)"

GOOOOD
javascript:addMarkerForPlace({
  "adminCode1" : "42",
  "lng" : 23.32415,
  "geonameId" : "727011",
  "toponymName" : "Sofia",
  "countryId" : "732800",
  "fcl" : "P",
  "population" : 1152556,
  "countryCode" : "BG",
  "name" : "Sofia",
  "fclName" : "city, village,...",
  "adminCodes" : null,
  "countryName" : "Bulgaria",
  "fcodeName" : "capital of a political entity",
  "adminName1" : "Sofia-Capital",
  "lat" : 42.69751,
  "fcode" : "PPLC",
  "location" : "42.69751, 23.32415"
});


<a  href="#"
    onclick="javascript:addMarkerForPlace(&#39;{
  &quot;adminCode1&quot; : &quot;42&quot;,
  &quot;lng&quot; : 23.32415,
  &quot;geonameId&quot; : &quot;727011&quot;,
  &quot;toponymName&quot; : &quot;Sofia&quot;,
  &quot;countryId&quot; : &quot;732800&quot;,
  &quot;fcl&quot; : &quot;P&quot;,
  &quot;population&quot; : 1152556,
  &quot;countryCode&quot; : &quot;BG&quot;,
  &quot;name&quot; : &quot;Sofia&quot;,
  &quot;fclName&quot; : &quot;city, village,...&quot;,
  &quot;adminCodes&quot; : null,
  &quot;countryName&quot; : &quot;Bulgaria&quot;,
  &quot;fcodeName&quot; : &quot;capital of a political entity&quot;,
  &quot;adminName1&quot; : &quot;Sofia-Capital&quot;,
  &quot;lat&quot; : 42.69751,
  &quot;fcode&quot; : &quot;PPLC&quot;,
  &quot;location&quot; : &quot;42.69751, 23.32415&quot;
}&#39;)">

onclick=javascript:addMarkerForPlace(&#39;{
  &quot;adminCode1&quot; : &quot;42&quot;,
  &quot;lng&quot; : 23.32415,
  &quot;geonameId&quot; : &quot;727011&quot;,
  &quot;toponymName&quot; : &quot;Sofia&quot;,
  &quot;countryId&quot; : &quot;732800&quot;,
  &quot;fcl&quot; : &quot;P&quot;,
  &quot;population&quot; : 1152556,
  &quot;countryCode&quot; : &quot;BG&quot;,
  &quot;name&quot; : &quot;Sofia&quot;,
  &quot;fclName&quot; : &quot;city, village,...&quot;,
  &quot;adminCodes&quot; : null,
  &quot;countryName&quot; : &quot;Bulgaria&quot;,
  &quot;fcodeName&quot; : &quot;capital of a political entity&quot;,
  &quot;adminName1&quot; : &quot;Sofia-Capital&quot;,
  &quot;lat&quot; : 42.69751,
  &quot;fcode&quot; : &quot;PPLC&quot;,
  &quot;location&quot; : &quot;42.69751, 23.32415&quot;
}&#39;)"

